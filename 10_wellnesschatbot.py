def check_word(input_string):
    return 'stressed' in input_string.lower()

def response_handler(found_word):
    if found_word:
        return 'its ok u can afford to rest'
    else:
        return 'ok then rmb to vote tmr'

def chatbot_logic():
    #user input
    input_string = input('how r u feeling today\n')

    while 'bye' not in input_string:
        
        #check input for keyword
        found_word = check_word(input_string)

        #manipulate + update w response
        response = response_handler(found_word)

        print(response)

        input_string = input('how r u feeling today\n')

chatbot_logic()
