my_set = set()

my_set.add(1)
my_set.add(2)
my_set.add(3)

print(my_set)

#set is very different data structure from list;
#to remove duplicate entries from list

my_list = [1, 1, 1, 2, 2, 3, 3, 3]
my_newset = set(my_list)
print(my_newset)
