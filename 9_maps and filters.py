#LAMDA, map() and filter() functions go together
#it is a built-in Python function

my_nums = [1, 2, 3, 4, 5]
squared_nums = [ ]

def square(num):
    return num**2

#iterate through list
#for item in my_nums:
#    print(square(item))

#for item in map(square, my_nums):
#    print(item)

#now need to append

for item in my_nums:
    squared_nums.append(square(item))

print(squared_nums)


---

my_nums = [1, 2, 3, 4, 5]

def square(num):
    return num**2

print(list(map(square, my_nums)))
#map function helps to make your code more efficient,
#rather than the number of lines needed for a for loop

---

name = ['clarice', 'valerie', 'zeharn']

def splicer(my_string):
    if len(my_string)%2 == 0:
        return 'EVEN'
    else:
        return my_string[0]

print(list(map(splicer, name)))
#don't put brackets after the name of your function

#filter works almost the same way as map function

---

my_nums = [1, 2, 3, 4, 5, 6]

def check_even(num):
    return num%2 == 0

print(list(filter(check_even, my_nums)))

#if you wanna multiply everything by two, use map
#if you only want to select some responses that you get in your list,
#use filter. it's either ALL or SOME. that's the diff between
#map and filter
