#INDEXING NUMBERS
#h  e  l  l  o
#0  1  2  3  4
#0 -4 -3 -2 -1
#The negative numbers is to help rearrange strings from backwards to forwards,
#it's less common

mystring = "abcdefghijk"

#brackets refer to something at a certain index

print(mystring[5])

#STRING SLICING: HOW TO GET A SEQUENCE OUT OFF THE STRING

# mystring [start:end:increment]
# a=0, and is NOT inclusive of the number you put in. k=7.
# Increment is automatically set to 1

print(mystring[:3])

#FOR LOOP
#for (i = 0; i < 10; i++) {
#xxx
#}

print(mystring[3:6])
print(mystring[::-1])

print(mystring[2:9:3])
print(mystring[2::3])

