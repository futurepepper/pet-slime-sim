# first letter + spell backwards (johann = nnahoj)

def secret_code(word):
    first_letter = word[0]
    other_letters = word[:0:-1]

    return first_letter + other_letters
