#my_list = [1, 2, 3]

#my_list.append(4)

#print(my_list)

#methods and functions are almost the same; though function can exist on its own
#append is a method, and methods must be attached to an object; object oriented

#go to python documentation page; you can reference that list. docs.python.org;
#reference; shows everything u can use

#syntax of a basic function starts with def

def hello_function(name = 'NAME'):
    '''
    returns a greeting to a name
    
    :param name: string
    :type name: string
    :return: "hello" appended to name
  
    '''
    #print('hello', name)
    return 'hello' + name
    
#always use camelcase for function names
#good to add a DOCSTRING
#always have a return with function; just need a result from running a function
