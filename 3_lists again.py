#Data structure: A list. Other languages calls it an array.
#it's good to put your variables together

tea_1 = "earl grey"
tea_2 = "pu er"
tea_3 = "oolong"
tea_4 = "matcha"

tea_list = ["earl grey", "pu er", "oolong", "matcha"]

my_list = ["one", "two", "three", "four"]
another_list = ["four", "five"]

new_list = my_list + another_list
new_list[0] = "WOW"
print(new_list)



nested_list = [["clrs", 1], ["velerie", 2], ["Triciaaaa",3]]
nested_list[2][0]
