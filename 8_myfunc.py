#def my_func(a, b, c, d):
#    return sum ((a, b, c, d))

#star args
#def my_func(*args):
#    return sum(args)

#args is a tuple, so you don't need to add brackets

#def my_func(**kwargs):
#    if 'bestest' in kwargs:
#        print('the bestest person is {}'.format(kwargs['bestest']))
#    else:
#        print('no one found')

#kwargs is dictionary

def check_even(*args):
    return [num for num in args if num%2 == 0]
