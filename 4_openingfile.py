#always close your files!
#these can go into shell
my_file = open("my_file.txt")

#instead of moving your file, you can also place the entire file path in here.

my_file = open("/Users/Johann/Dropbox/Python/my_file.txt")

print(my_file.read())

my_file.seek(0)

my_file.close()

with open('my_file.txt') as my_new_file:
	print(my_new_file.read())
#the file is only open when you call this whole statement together.
#if you want to call the file again you must open it again.

with open('my_file.txt', mode = 'r') as my_new_file:
	print(my_new_file.read())
#modes r = read

 with open('my_file.txt', mode = 'a') as my_new_file:
	print(my_new_file.write('append'))

#r is read, w is write, r+, w+ (read and write) plus will
#overwrite the entire file, a is append

	with open('my_file.txt', mode = 'w+') as my_new_file:
	my_new_file.write('write')
	my_new_file.seek(0)
	my_new_file.read()

#these are useful for reading from an API,
    #for example trying to find a trending tweet
