#Data structure: A list. Other languages calls it an array.
#it's good to put your variables together

tea_1 = "earl grey"
tea_2 = "pu er"
tea_3 = "oolong"
tea_4 = "matcha"

tea_list = ["earl grey", "pu er", "oolong", "matcha"]

my_list = ["one", "two", "three", "four"]

print(my_list[0])
