#LAMBDA expressions- a quick way to create anonymous functions
#anonymous means it doesn't have a name
#nameless, single, one-time use functions that don't need a keyword to be
#referenced, as they will only be used once

#lambda num:num**2

#my_nums = [1,2,3,4,5]
#print(list(map(lambda num:num**2, my_nums)))

#tiresome to create a function that's only used once; increases
#the performance of your code

x = 25

def changer():
    x = 50
    return x
# Local
# Enclosing function locals
# Global
# Built-in
