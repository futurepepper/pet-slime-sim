my_dict = {"key 1": "value 1", "key 2": "value 2", "key 3": "value 3"}
print(my_dict.keys())
print(my_dict.values())
print(my_dict.items())
#key = a keyword to look up a value by.

#fruits = {"apple": 0.50, "orange": 0.80}
#fruit["orange"]

#list organises by sequence, dictionary organises by keys
