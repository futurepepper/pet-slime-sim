# USING , AND +
print("hello", "world", "i'm", "dead", "and", "gay")

print("im"+" cheating "+"haha")

# WHEN YOU NEED TO USE "" INSIDE A STRING
print('"hello world," she said.')

# CONCATENATIONS
print("\"Yes,\" the world replied.")

print("hi " * 3)

# VARIABLES
name = "johann "
status = name + "is the biggest clown existing within this reality"
print(status)

# ABOVE IS NOOB METHOD, THE PROPER IS STRING FORMAT
name = "johann"
final_form = "clown"
status = "{} is the biggest {} existing within this reality".format(name, final_form)
print(status)

# HOW TO RUN AND STORE PROGRAMMES
# WHAT'S THE DIFFERENCE BETWEEN SHELL
# INTEGERS AND VARIABLES
