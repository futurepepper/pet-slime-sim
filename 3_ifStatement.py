username = input("Please enter your username: ")
password = input("Please enter your password: ")

#COMPARISON OPERATOR (==) vs ASSIGNMENT OPERATOR (=)
#if username == "valerie":
#    if password == "ee":
#        print("Welcome, {}".format(username))

if username == "valerie" and password == "ee":
    print ("Welcome, {}".format(username))

#if username == "valerie" or password == "ee":
#    print ("Welcome, {}".format(username))

else:
    print("You are not welcome here!!!")

#always do an else statement,
#so you know which part of your code is working/not working

#middle ground between if/else
elif:
