johanns_playlist = {'song 1': {'Title': 'Creep',
                               'Artist': 'Radiohead',
                               'Album': 'Pablo Honey'},
                    'song 2': {'Title': 'Smile',
                               'Artist': 'Judy Garland',
                               'Album': 'Greatest Hits'},
                    'song 3': {'Title': 'My Way',
                               'Artist': 'Frank Sinatra',
                               'Album': 'My Way: 40th Anniversary Edition'}}

johanns_playlist['song 1']['Album'] = johanns_playlist['song 1']['Album'].upper()
johanns_playlist['song 2']['Title'] = "Somewhere Over the Rainbow"


print(johanns_playlist)

#print(johanns_playlist['song 2']['Album'])


