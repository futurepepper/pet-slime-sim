#multiple functions; guess-the-cup game
#shuffle cups, handle user guest

#cups and balls
game_setup = ['', '0', '']

#shuffle function; need to import this from the random library
from random import shuffle

def shuffle_list(game_list):
    shuffle(game_list)
    return game_list

def player_guess():
    guess = ''
    guess = input('pick a number: 0, 1, 2\n')
    return int(guess)


#initial list
game_setup = ['', '0', '']

#shuffle list
game_case = shuffle_list(game_setup)

#user guess
guess = player_guess()

#check guess
check_guess(game_case, guess)
