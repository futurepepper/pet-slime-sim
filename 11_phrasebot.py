#Phrasebot

from random import randint

phrases = [ ]
user_response = input('phrasebot >> hello i say to u what u said before\nyou >> ')
prev_index = 0

while True:
    phrases.append(user_response)
    rand_index = randint(0, len(phrases)-1)

    if len(phrases) > 2:
        while rand_index == len(phrases)-1 or rand_index == prev_index:
            rand_index = randint(0, len(phrases)-1)
    
    phrasebot_response = 'phrasebot >> ' + phrases[rand_index]
    print(phrasebot_response)

    prev_index = rand_index
    user_response = input ('you >> ')
