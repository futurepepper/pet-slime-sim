#for loop (definite loop); while loop (indefinite loop)
#many objects in python are iterable; can go through every object

#list_to_iterate = [1, 2, 3, 4, 5]
#for hjkl in list_to_iterate:
    #print(hjkl)

#can call hjkl anything you want, it's just a placeholder variable


#num_list = [1, 2, 3, 4, 5]
#for num in num_list:
    #print(num)

#what happens when placeholder variable is different from what's printed

#num_list = [1, 2, 3, 4, 5]
#for num in num_list:
    #print('omg')

#for letter in 'Hello World':
    #print(letter)

#use for loop to go through everything in a list

    


#num_list = [1, 2, 3, 4, 5, 6]

#for num in num_list:
    #if num % 2 == 0:
    # check for even num
    #a modular operator that divides your number by two; if there's no
    #remainder, it'll be 0. if you divide even number by two, remainder =0.
        #print(num)

#A tuple is a collection which is ordered and unchangeable.
#In Python tuples are written with round brackets.

#tup = (1, 2, 3)
#for item in tup:
#    print(item)

#my_list = [(1, 2), (3, 4), (5, 6)]
#(1,2) is one item
#you'll never be able to access the numbers inside the tuple, unless...

#for item in my_list:
#    print(item)

#for a, b in my_list:
##    print(a)
#    print(b)

#a,b is similar to your key value pair in your dictionary

#d = {'k1': 1, 'k2': 2, 'k3': 3}

#for item in d:
#    print(item)

#this way you are only printing keys (k1), not the value (1)

#for item in d.items():
#    print(item)

    #this produces tuples

#for key, value in d.items():
#    print(key)
#    print(value)

#for item in d.values():
#    print(item)
    
    #this is the built-in method; the previous one has more flexibility
    #since you can choose if you want key or value

#while bool_comparison:
    #do something
#else:
    #do something else

x = 0

while x < 5:
    print(x)
    x += 1
    #sometimes people do as x += 1; is the same as x = x + 1; in other
    #languages, it is x ++
    # the loop x+1 is needed to prevent an infinite loop: or else x
    # will remain the same
else:
    print('oh no! x is now bigger than 5!')

#if you ever accidentally make an infinite while loop, restart shell
    #while loop when u don't know how many times a loop has to run
