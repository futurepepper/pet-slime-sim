#STRINGS ARE IMMUTABLE - SOMETHING THAT CANNOT BE REARRANGED OR CHANGED.

name = "gerard way"

#To replace a letter in the string, do concatenation: + and ,

print(name[:2] + "q")

#Methods have to be closed with circle brackets
print(name.upper())

print(name.split())

#For input function, you can type into the brackets, like print function.
#name = input("Type in your name here: ")
#print("Hello {}! It's nice to meet you. Please destroy me!".format(name))

print("What is your name?")
name = input()
print("Hello {}, destroy me".format(name))
