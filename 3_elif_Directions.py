direction = input("Please input a direction (N, S, E, W): ")

if direction == "N":
        print("Heading {}".format(direction))

elif direction == "S":
    print("Heading South")

elif direction == "E":
    print("Heading East")

elif direction == "W":
    print("Heading West")

else:
    print("Not a valid direction")
